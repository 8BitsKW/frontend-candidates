
(function(l, r) { if (!l || l.getElementById('livereloadscript')) return; r = l.createElement('script'); r.async = 1; r.src = '//' + (self.location.host || 'localhost').split(':')[0] + ':35729/livereload.js?snipver=1'; r.id = 'livereloadscript'; l.getElementsByTagName('head')[0].appendChild(r) })(self.document);
var app = (function () {
    'use strict';

    function noop() { }
    function add_location(element, file, line, column, char) {
        element.__svelte_meta = {
            loc: { file, line, column, char }
        };
    }
    function run(fn) {
        return fn();
    }
    function blank_object() {
        return Object.create(null);
    }
    function run_all(fns) {
        fns.forEach(run);
    }
    function is_function(thing) {
        return typeof thing === 'function';
    }
    function safe_not_equal(a, b) {
        return a != a ? b == b : a !== b || ((a && typeof a === 'object') || typeof a === 'function');
    }
    let src_url_equal_anchor;
    function src_url_equal(element_src, url) {
        if (!src_url_equal_anchor) {
            src_url_equal_anchor = document.createElement('a');
        }
        src_url_equal_anchor.href = url;
        return element_src === src_url_equal_anchor.href;
    }
    function is_empty(obj) {
        return Object.keys(obj).length === 0;
    }
    function append(target, node) {
        target.appendChild(node);
    }
    function insert(target, node, anchor) {
        target.insertBefore(node, anchor || null);
    }
    function detach(node) {
        node.parentNode.removeChild(node);
    }
    function element(name) {
        return document.createElement(name);
    }
    function text(data) {
        return document.createTextNode(data);
    }
    function space() {
        return text(' ');
    }
    function attr(node, attribute, value) {
        if (value == null)
            node.removeAttribute(attribute);
        else if (node.getAttribute(attribute) !== value)
            node.setAttribute(attribute, value);
    }
    function children(element) {
        return Array.from(element.childNodes);
    }
    function set_style(node, key, value, important) {
        node.style.setProperty(key, value, important ? 'important' : '');
    }
    function custom_event(type, detail, bubbles = false) {
        const e = document.createEvent('CustomEvent');
        e.initCustomEvent(type, bubbles, false, detail);
        return e;
    }

    let current_component;
    function set_current_component(component) {
        current_component = component;
    }

    const dirty_components = [];
    const binding_callbacks = [];
    const render_callbacks = [];
    const flush_callbacks = [];
    const resolved_promise = Promise.resolve();
    let update_scheduled = false;
    function schedule_update() {
        if (!update_scheduled) {
            update_scheduled = true;
            resolved_promise.then(flush);
        }
    }
    function add_render_callback(fn) {
        render_callbacks.push(fn);
    }
    let flushing = false;
    const seen_callbacks = new Set();
    function flush() {
        if (flushing)
            return;
        flushing = true;
        do {
            // first, call beforeUpdate functions
            // and update components
            for (let i = 0; i < dirty_components.length; i += 1) {
                const component = dirty_components[i];
                set_current_component(component);
                update(component.$$);
            }
            set_current_component(null);
            dirty_components.length = 0;
            while (binding_callbacks.length)
                binding_callbacks.pop()();
            // then, once components are updated, call
            // afterUpdate functions. This may cause
            // subsequent updates...
            for (let i = 0; i < render_callbacks.length; i += 1) {
                const callback = render_callbacks[i];
                if (!seen_callbacks.has(callback)) {
                    // ...so guard against infinite loops
                    seen_callbacks.add(callback);
                    callback();
                }
            }
            render_callbacks.length = 0;
        } while (dirty_components.length);
        while (flush_callbacks.length) {
            flush_callbacks.pop()();
        }
        update_scheduled = false;
        flushing = false;
        seen_callbacks.clear();
    }
    function update($$) {
        if ($$.fragment !== null) {
            $$.update();
            run_all($$.before_update);
            const dirty = $$.dirty;
            $$.dirty = [-1];
            $$.fragment && $$.fragment.p($$.ctx, dirty);
            $$.after_update.forEach(add_render_callback);
        }
    }
    const outroing = new Set();
    let outros;
    function transition_in(block, local) {
        if (block && block.i) {
            outroing.delete(block);
            block.i(local);
        }
    }
    function transition_out(block, local, detach, callback) {
        if (block && block.o) {
            if (outroing.has(block))
                return;
            outroing.add(block);
            outros.c.push(() => {
                outroing.delete(block);
                if (callback) {
                    if (detach)
                        block.d(1);
                    callback();
                }
            });
            block.o(local);
        }
    }
    function create_component(block) {
        block && block.c();
    }
    function mount_component(component, target, anchor, customElement) {
        const { fragment, on_mount, on_destroy, after_update } = component.$$;
        fragment && fragment.m(target, anchor);
        if (!customElement) {
            // onMount happens before the initial afterUpdate
            add_render_callback(() => {
                const new_on_destroy = on_mount.map(run).filter(is_function);
                if (on_destroy) {
                    on_destroy.push(...new_on_destroy);
                }
                else {
                    // Edge case - component was destroyed immediately,
                    // most likely as a result of a binding initialising
                    run_all(new_on_destroy);
                }
                component.$$.on_mount = [];
            });
        }
        after_update.forEach(add_render_callback);
    }
    function destroy_component(component, detaching) {
        const $$ = component.$$;
        if ($$.fragment !== null) {
            run_all($$.on_destroy);
            $$.fragment && $$.fragment.d(detaching);
            // TODO null out other refs, including component.$$ (but need to
            // preserve final state?)
            $$.on_destroy = $$.fragment = null;
            $$.ctx = [];
        }
    }
    function make_dirty(component, i) {
        if (component.$$.dirty[0] === -1) {
            dirty_components.push(component);
            schedule_update();
            component.$$.dirty.fill(0);
        }
        component.$$.dirty[(i / 31) | 0] |= (1 << (i % 31));
    }
    function init(component, options, instance, create_fragment, not_equal, props, append_styles, dirty = [-1]) {
        const parent_component = current_component;
        set_current_component(component);
        const $$ = component.$$ = {
            fragment: null,
            ctx: null,
            // state
            props,
            update: noop,
            not_equal,
            bound: blank_object(),
            // lifecycle
            on_mount: [],
            on_destroy: [],
            on_disconnect: [],
            before_update: [],
            after_update: [],
            context: new Map(parent_component ? parent_component.$$.context : options.context || []),
            // everything else
            callbacks: blank_object(),
            dirty,
            skip_bound: false,
            root: options.target || parent_component.$$.root
        };
        append_styles && append_styles($$.root);
        let ready = false;
        $$.ctx = instance
            ? instance(component, options.props || {}, (i, ret, ...rest) => {
                const value = rest.length ? rest[0] : ret;
                if ($$.ctx && not_equal($$.ctx[i], $$.ctx[i] = value)) {
                    if (!$$.skip_bound && $$.bound[i])
                        $$.bound[i](value);
                    if (ready)
                        make_dirty(component, i);
                }
                return ret;
            })
            : [];
        $$.update();
        ready = true;
        run_all($$.before_update);
        // `false` as a special case of no DOM component
        $$.fragment = create_fragment ? create_fragment($$.ctx) : false;
        if (options.target) {
            if (options.hydrate) {
                const nodes = children(options.target);
                // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
                $$.fragment && $$.fragment.l(nodes);
                nodes.forEach(detach);
            }
            else {
                // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
                $$.fragment && $$.fragment.c();
            }
            if (options.intro)
                transition_in(component.$$.fragment);
            mount_component(component, options.target, options.anchor, options.customElement);
            flush();
        }
        set_current_component(parent_component);
    }
    /**
     * Base class for Svelte components. Used when dev=false.
     */
    class SvelteComponent {
        $destroy() {
            destroy_component(this, 1);
            this.$destroy = noop;
        }
        $on(type, callback) {
            const callbacks = (this.$$.callbacks[type] || (this.$$.callbacks[type] = []));
            callbacks.push(callback);
            return () => {
                const index = callbacks.indexOf(callback);
                if (index !== -1)
                    callbacks.splice(index, 1);
            };
        }
        $set($$props) {
            if (this.$$set && !is_empty($$props)) {
                this.$$.skip_bound = true;
                this.$$set($$props);
                this.$$.skip_bound = false;
            }
        }
    }

    function dispatch_dev(type, detail) {
        document.dispatchEvent(custom_event(type, Object.assign({ version: '3.41.0' }, detail), true));
    }
    function append_dev(target, node) {
        dispatch_dev('SvelteDOMInsert', { target, node });
        append(target, node);
    }
    function insert_dev(target, node, anchor) {
        dispatch_dev('SvelteDOMInsert', { target, node, anchor });
        insert(target, node, anchor);
    }
    function detach_dev(node) {
        dispatch_dev('SvelteDOMRemove', { node });
        detach(node);
    }
    function attr_dev(node, attribute, value) {
        attr(node, attribute, value);
        if (value == null)
            dispatch_dev('SvelteDOMRemoveAttribute', { node, attribute });
        else
            dispatch_dev('SvelteDOMSetAttribute', { node, attribute, value });
    }
    function validate_slots(name, slot, keys) {
        for (const slot_key of Object.keys(slot)) {
            if (!~keys.indexOf(slot_key)) {
                console.warn(`<${name}> received an unexpected slot "${slot_key}".`);
            }
        }
    }
    /**
     * Base class for Svelte components with some minor dev-enhancements. Used when dev=true.
     */
    class SvelteComponentDev extends SvelteComponent {
        constructor(options) {
            if (!options || (!options.target && !options.$$inline)) {
                throw new Error("'target' is a required option");
            }
            super();
        }
        $destroy() {
            super.$destroy();
            this.$destroy = () => {
                console.warn('Component was already destroyed'); // eslint-disable-line no-console
            };
        }
        $capture_state() { }
        $inject_state() { }
    }

    /* src\Readme.svelte generated by Svelte v3.41.0 */

    const file = "src\\Readme.svelte";

    function create_fragment$1(ctx) {
    	let div0;
    	let t0;
    	let div12;
    	let p0;
    	let img0;
    	let img0_src_value;
    	let t1;
    	let h10;
    	let t3;
    	let hr0;
    	let t4;
    	let div1;
    	let p1;
    	let t6;
    	let p2;
    	let t7;
    	let a0;
    	let t9;
    	let t10;
    	let p3;
    	let t12;
    	let h20;
    	let t14;
    	let div2;
    	let p4;
    	let t15;
    	let a1;
    	let t17;
    	let p5;
    	let t19;
    	let pre0;
    	let code0;
    	let t20;
    	let span0;
    	let t22;
    	let p6;
    	let t24;
    	let pre1;
    	let code1;
    	let t25;
    	let span1;
    	let span2;
    	let t28;
    	let p7;
    	let t29;
    	let a2;
    	let t31;
    	let t32;
    	let h21;
    	let t34;
    	let div11;
    	let p8;
    	let img1;
    	let img1_src_value;
    	let t35;
    	let p9;
    	let t37;
    	let p10;
    	let t39;
    	let p11;
    	let t41;
    	let h30;
    	let t43;
    	let p12;
    	let t44;
    	let a3;
    	let t46;
    	let p13;
    	let t48;
    	let h31;
    	let t50;
    	let p14;
    	let t52;
    	let ul0;
    	let li0;
    	let t53;
    	let code2;
    	let t55;
    	let pre2;
    	let code3;
    	let div3;
    	let span3;
    	let t57;
    	let span4;
    	let t59;
    	let t60;
    	let div4;
    	let span5;
    	let t62;
    	let span6;
    	let t64;
    	let t65;
    	let div5;
    	let span7;
    	let t67;
    	let span8;
    	let t69;
    	let t70;
    	let div6;
    	let span9;
    	let t72;
    	let span10;
    	let t74;
    	let t75;
    	let div7;
    	let span11;
    	let t77;
    	let span12;
    	let t79;
    	let t80;
    	let div8;
    	let span13;
    	let t82;
    	let span14;
    	let t84;
    	let t85;
    	let div9;
    	let span15;
    	let t87;
    	let span16;
    	let t89;
    	let t90;
    	let div10;
    	let span17;
    	let t92;
    	let span18;
    	let t94;
    	let t95;
    	let li1;
    	let t97;
    	let li2;
    	let t99;
    	let h32;
    	let t101;
    	let p15;
    	let t103;
    	let ul1;
    	let li3;
    	let t105;
    	let li4;
    	let t107;
    	let li5;
    	let t109;
    	let h33;
    	let t111;
    	let p16;
    	let t113;
    	let ul2;
    	let li6;
    	let t115;
    	let li7;
    	let t117;
    	let li8;
    	let t119;
    	let li9;
    	let t121;
    	let h34;
    	let t123;
    	let ul3;
    	let li10;
    	let t125;
    	let pre3;
    	let code4;
    	let span19;
    	let t127;
    	let t128;
    	let pre4;
    	let code5;
    	let span20;
    	let t130;
    	let span21;
    	let t132;
    	let pre5;
    	let code6;
    	let t133;
    	let span22;
    	let t135;
    	let span23;
    	let t137;
    	let span24;
    	let t139;
    	let h35;
    	let t141;
    	let p17;
    	let t143;
    	let h22;
    	let t145;
    	let hr1;
    	let t146;
    	let ol;
    	let li11;
    	let t148;
    	let li12;
    	let t150;
    	let li13;
    	let t152;
    	let li14;
    	let t154;
    	let li15;
    	let t156;
    	let p18;
    	let t158;
    	let hr2;
    	let t159;
    	let h23;
    	let t161;
    	let hr3;
    	let t162;
    	let p19;
    	let t164;
    	let p20;
    	let t166;
    	let h11;

    	const block = {
    		c: function create() {
    			div0 = element("div");
    			t0 = space();
    			div12 = element("div");
    			p0 = element("p");
    			img0 = element("img");
    			t1 = space();
    			h10 = element("h1");
    			h10.textContent = "Welcome BITS Candidates";
    			t3 = space();
    			hr0 = element("hr");
    			t4 = space();
    			div1 = element("div");
    			p1 = element("p");
    			p1.textContent = "If you're reading this, you've passed your interview, so Congrats! Now its time\r\n            to show us that you can code and can fill the backend position available.";
    			t6 = space();
    			p2 = element("p");
    			t7 = text("The task is simple, build an endpoint/service using PHP with no external dependencies\r\n            other than the ones provided: ");
    			a0 = element("a");
    			a0.textContent = "Slim Framework 4";
    			t9 = text(". This means you really have to think about your code and how to structure the app.\r\n            Laravel will not come to your rescue.");
    			t10 = space();
    			p3 = element("p");
    			p3.textContent = "We are not reeinventing the wheel here, we just need to see your understanding of what\r\n            \"good code\" is and how you approach the design of your software. Anyone can\r\n            write code, but only good developers can write good and well designed code. Also,\r\n            we've taken you out of your comfort zone.";
    			t12 = space();
    			h20 = element("h2");
    			h20.textContent = "Preparing The Environment";
    			t14 = space();
    			div2 = element("div");
    			p4 = element("p");
    			t15 = text("This project requires the latest LTS version of NodeJS. The framework of choice for this\r\n            task and in Bits is ");
    			a1 = element("a");
    			a1.textContent = "Svelte";
    			t17 = space();
    			p5 = element("p");
    			p5.textContent = "After you have cloned this repo and install the yarn, install the dependencies with:";
    			t19 = space();
    			pre0 = element("pre");
    			code0 = element("code");
    			t20 = text("npm ");
    			span0 = element("span");
    			span0.textContent = "install";
    			t22 = space();
    			p6 = element("p");
    			p6.textContent = "You can then start the application running:";
    			t24 = space();
    			pre1 = element("pre");
    			code1 = element("code");
    			t25 = text("npm ");
    			span1 = element("span");
    			span1.textContent = "run ";
    			span2 = element("span");
    			span2.textContent = "dev";
    			t28 = space();
    			p7 = element("p");
    			t29 = text("That's it. Just Access ");
    			a2 = element("a");
    			a2.textContent = "http://localhost:5000";
    			t31 = text(" in your browser.");
    			t32 = space();
    			h21 = element("h2");
    			h21.textContent = "The Task";
    			t34 = space();
    			div11 = element("div");
    			p8 = element("p");
    			img1 = element("img");
    			t35 = space();
    			p9 = element("p");
    			p9.textContent = "You will build a screen where the user wants to apply for a loan, but before they do\r\n            that, they want to know how much interest and the monthly installments will be for their\r\n            loan";
    			t37 = space();
    			p10 = element("p");
    			p10.textContent = "In the screen, you will build a calculator in which, the users choose (i) the loan type\r\n            they want to apply for, (ii) the amount they want to apply for, (iii) and the number of\r\n            months to repay the loan.";
    			t39 = space();
    			p11 = element("p");
    			p11.textContent = "When the users changes the value of any of the inputs, the monthly installment value,\r\n            the interest value and the total loan amount is calculated and displayed to them.";
    			t41 = space();
    			h30 = element("h3");
    			h30.textContent = "Assets";
    			t43 = space();
    			p12 = element("p");
    			t44 = text("You can find the layout mockups here on our Figma project:\r\n            ");
    			a3 = element("a");
    			a3.textContent = "Layout mockups";
    			t46 = space();
    			p13 = element("p");
    			p13.textContent = "Once you have opened the link you must sign up and log in so you can have access to the\r\n            colors, fonts, margins and assets information.";
    			t48 = space();
    			h31 = element("h3");
    			h31.textContent = "Products";
    			t50 = space();
    			p14 = element("p");
    			p14.textContent = "The products component should:";
    			t52 = space();
    			ul0 = element("ul");
    			li0 = element("li");
    			t53 = text("Be fetched from ");
    			code2 = element("code");
    			code2.textContent = "/products.json";
    			t55 = text(". Each product definition is as follows:\r\n                {\r\n                ");
    			pre2 = element("pre");
    			code3 = element("code");
    			div3 = element("div");
    			span3 = element("span");
    			span3.textContent = "\"id\"";
    			t57 = text(": ");
    			span4 = element("span");
    			span4.textContent = "\"product id\"";
    			t59 = text(",");
    			t60 = text("\r\n                        ");
    			div4 = element("div");
    			span5 = element("span");
    			span5.textContent = "\"interest\"";
    			t62 = text(": ");
    			span6 = element("span");
    			span6.textContent = "\"The rate of interest\"";
    			t64 = text(",");
    			t65 = text("\r\n                        ");
    			div5 = element("div");
    			span7 = element("span");
    			span7.textContent = "\"namr\"";
    			t67 = text(": ");
    			span8 = element("span");
    			span8.textContent = "\"Product name\"";
    			t69 = text(",");
    			t70 = text("\r\n                        ");
    			div6 = element("div");
    			span9 = element("span");
    			span9.textContent = "\"min_amount\"";
    			t72 = text(": ");
    			span10 = element("span");
    			span10.textContent = "\"Minimum amount allowed to be loaned\"";
    			t74 = text(",");
    			t75 = text("\r\n                        ");
    			div7 = element("div");
    			span11 = element("span");
    			span11.textContent = "\"max_amount\"";
    			t77 = text(": ");
    			span12 = element("span");
    			span12.textContent = "\"Max amount allowed to be loaned\"";
    			t79 = text(",");
    			t80 = text("\r\n                        ");
    			div8 = element("div");
    			span13 = element("span");
    			span13.textContent = "\"min_tenure\"";
    			t82 = text(": ");
    			span14 = element("span");
    			span14.textContent = "\"Minimum number of months allowed\"";
    			t84 = text(",");
    			t85 = text("\r\n                        ");
    			div9 = element("div");
    			span15 = element("span");
    			span15.textContent = "\"max_tenure\"";
    			t87 = text(": ");
    			span16 = element("span");
    			span16.textContent = "\"Maximum number of months allowed\"";
    			t89 = text(",");
    			t90 = text("\r\n                        ");
    			div10 = element("div");
    			span17 = element("span");
    			span17.textContent = "\"image\"";
    			t92 = text(": ");
    			span18 = element("span");
    			span18.textContent = "\"Image to use as the icon\"";
    			t94 = text("\r\n                }");
    			t95 = space();
    			li1 = element("li");
    			li1.textContent = "Each product should be displayed using the product.image property";
    			t97 = space();
    			li2 = element("li");
    			li2.textContent = "When a product is changed, reset the loan and months to their respective minimums";
    			t99 = space();
    			h32 = element("h3");
    			h32.textContent = "Loan Amount input";
    			t101 = space();
    			p15 = element("p");
    			p15.textContent = "The money input component should:";
    			t103 = space();
    			ul1 = element("ul");
    			li3 = element("li");
    			li3.textContent = "Allow only numbers";
    			t105 = space();
    			li4 = element("li");
    			li4.textContent = "Display the value formatted as money (e.g 3500.45 should be 3,500.44)";
    			t107 = space();
    			li5 = element("li");
    			li5.textContent = "Respect the min and max amounts of the selected product";
    			t109 = space();
    			h33 = element("h3");
    			h33.textContent = "Months input";
    			t111 = space();
    			p16 = element("p");
    			p16.textContent = "The month input component should:";
    			t113 = space();
    			ul2 = element("ul");
    			li6 = element("li");
    			li6.textContent = "Allow only numbers";
    			t115 = space();
    			li7 = element("li");
    			li7.textContent = "When clicking on the arrow buttons it should go up and down by one";
    			t117 = space();
    			li8 = element("li");
    			li8.textContent = "On focused, the users should be able to increase/decrease by typing the Up and Down\r\n                arrow key on the keyboard respectively";
    			t119 = space();
    			li9 = element("li");
    			li9.textContent = "Respect the min and max tenure properties of the selected product";
    			t121 = space();
    			h34 = element("h3");
    			h34.textContent = "Description & Details";
    			t123 = space();
    			ul3 = element("ul");
    			li10 = element("li");
    			li10.textContent = "When the Amount or months changes perform the following calculation to get the total\r\n                amount:";
    			t125 = space();
    			pre3 = element("pre");
    			code4 = element("code");
    			span19 = element("span");
    			span19.textContent = "total amount";
    			t127 = text(" = loan amount + (loan amount * product interest)");
    			t128 = space();
    			pre4 = element("pre");
    			code5 = element("code");
    			span20 = element("span");
    			span20.textContent = "monthly";
    			t130 = text(" installment = total amount / ");
    			span21 = element("span");
    			span21.textContent = "# months";
    			t132 = space();
    			pre5 = element("pre");
    			code6 = element("code");
    			t133 = text("target ");
    			span22 = element("span");
    			span22.textContent = "month";
    			t135 = text(" = current ");
    			span23 = element("span");
    			span23.textContent = "month";
    			t137 = text(" + ");
    			span24 = element("span");
    			span24.textContent = "# months";
    			t139 = space();
    			h35 = element("h3");
    			h35.textContent = "Confirm button";
    			t141 = space();
    			p17 = element("p");
    			p17.textContent = "You don't need to add any action on the confirmation button";
    			t143 = space();
    			h22 = element("h2");
    			h22.textContent = "What will I be assessed on";
    			t145 = space();
    			hr1 = element("hr");
    			t146 = space();
    			ol = element("ol");
    			li11 = element("li");
    			li11.textContent = "If your code works";
    			t148 = space();
    			li12 = element("li");
    			li12.textContent = "Your application structure";
    			t150 = space();
    			li13 = element("li");
    			li13.textContent = "How close your page is to the mockups, both on mobile & desktop";
    			t152 = space();
    			li14 = element("li");
    			li14.textContent = "How clean and organized your code is";
    			t154 = space();
    			li15 = element("li");
    			li15.textContent = "How long it took you to complete the task";
    			t156 = space();
    			p18 = element("p");
    			p18.textContent = "   ";
    			t158 = space();
    			hr2 = element("hr");
    			t159 = space();
    			h23 = element("h2");
    			h23.textContent = "Questions";
    			t161 = space();
    			hr3 = element("hr");
    			t162 = space();
    			p19 = element("p");
    			p19.textContent = "You can share any question/concern you have on teams. Not all your questions maybe answered,\r\n        sometimes you may have to assume and justify your decisions.";
    			t164 = space();
    			p20 = element("p");
    			p20.textContent = "   ";
    			t166 = space();
    			h11 = element("h1");
    			h11.textContent = "Good Luck!";
    			attr_dev(div0, "class", "fixed top-0 left-0 right-0 bottom-0  bg-gray-100");
    			set_style(div0, "z-index", "-1");
    			add_location(div0, file, 10, 0, 113);
    			attr_dev(img0, "class", "inline-block");
    			attr_dev(img0, "alt", "logo");
    			if (!src_url_equal(img0.src, img0_src_value = "/logo.png")) attr_dev(img0, "src", img0_src_value);
    			add_location(img0, file, 13, 27, 301);
    			attr_dev(p0, "class", "text-center svelte-t3irhz");
    			add_location(p0, file, 13, 4, 278);
    			attr_dev(h10, "class", "text-center text-3xl my-5");
    			add_location(h10, file, 14, 4, 366);
    			add_location(hr0, file, 15, 4, 438);
    			attr_dev(p1, "class", "svelte-t3irhz");
    			add_location(p1, file, 18, 8, 510);
    			attr_dev(a0, "href", "https://www.slimframework.com/docs/v4/");
    			add_location(a0, file, 24, 42, 871);
    			attr_dev(p2, "class", "svelte-t3irhz");
    			add_location(p2, file, 22, 8, 725);
    			attr_dev(p3, "class", "svelte-t3irhz");
    			add_location(p3, file, 29, 8, 1130);
    			attr_dev(div1, "class", "my-5 border-b border-gray-300 py-5");
    			add_location(div1, file, 17, 4, 452);
    			attr_dev(h20, "class", "font-bold text-xl");
    			attr_dev(h20, "id", "preparing-the-environment");
    			add_location(h20, file, 37, 4, 1520);
    			attr_dev(a1, "class", "text-pink-600");
    			attr_dev(a1, "target", "_blank");
    			attr_dev(a1, "href", "http://svelte.dev");
    			add_location(a1, file, 41, 32, 1809);
    			attr_dev(p4, "class", "svelte-t3irhz");
    			add_location(p4, file, 39, 8, 1670);
    			attr_dev(p5, "class", "svelte-t3irhz");
    			add_location(p5, file, 45, 8, 1941);
    			attr_dev(span0, "class", "hljs-keyword");
    			add_location(span0, file, 47, 22, 2071);
    			attr_dev(code0, "class", "svelte-t3irhz");
    			add_location(code0, file, 47, 12, 2061);
    			add_location(pre0, file, 46, 8, 2042);
    			attr_dev(p6, "class", "svelte-t3irhz");
    			add_location(p6, file, 49, 8, 2145);
    			attr_dev(span1, "class", "hljs-keyword");
    			add_location(span1, file, 51, 22, 2234);
    			attr_dev(span2, "class", "bash");
    			add_location(span2, file, 51, 60, 2272);
    			attr_dev(code1, "class", "svelte-t3irhz");
    			add_location(code1, file, 51, 12, 2224);
    			add_location(pre1, file, 50, 8, 2205);
    			attr_dev(a2, "href", "http://localhost:5000");
    			attr_dev(a2, "class", "text-pink-600");
    			attr_dev(a2, "target", "_blank");
    			add_location(a2, file, 54, 39, 2379);
    			attr_dev(p7, "class", "svelte-t3irhz");
    			add_location(p7, file, 53, 8, 2335);
    			attr_dev(div2, "class", "border-b border-gray-300 py-5");
    			add_location(div2, file, 38, 4, 1617);
    			attr_dev(h21, "class", "font-bold text-xl my-3");
    			add_location(h21, file, 62, 4, 2590);
    			attr_dev(img1, "class", "w-1/2 inline-block border border-gray-300");
    			if (!src_url_equal(img1.src, img1_src_value = "/Desktop@2x.png")) attr_dev(img1, "src", img1_src_value);
    			attr_dev(img1, "alt", "Loan Plan Mockup Desktop");
    			add_location(img1, file, 65, 12, 2745);
    			attr_dev(p8, "class", "text-center pb-10 svelte-t3irhz");
    			add_location(p8, file, 64, 8, 2702);
    			attr_dev(p9, "class", "svelte-t3irhz");
    			add_location(p9, file, 71, 8, 2943);
    			attr_dev(p10, "class", "svelte-t3irhz");
    			add_location(p10, file, 76, 8, 3188);
    			attr_dev(p11, "class", "svelte-t3irhz");
    			add_location(p11, file, 81, 8, 3456);
    			attr_dev(h30, "class", "font-bold text-lg my-3  border-b border-gray-200");
    			attr_dev(h30, "id", "assets");
    			add_location(h30, file, 85, 8, 3677);
    			attr_dev(a3, "class", "text-pink-600");
    			attr_dev(a3, "href", "https://www.figma.com/file/yCUfV4uHFWrWKenma3FH23/Bits-Svelte-Frontend");
    			add_location(a3, file, 88, 12, 3860);
    			attr_dev(p12, "class", "svelte-t3irhz");
    			add_location(p12, file, 86, 8, 3771);
    			attr_dev(p13, "class", "svelte-t3irhz");
    			add_location(p13, file, 94, 8, 4071);
    			attr_dev(h31, "class", "font-bold text-lg my-3  border-b border-gray-200");
    			attr_dev(h31, "id", "products");
    			add_location(h31, file, 98, 8, 4259);
    			attr_dev(p14, "class", "svelte-t3irhz");
    			add_location(p14, file, 99, 8, 4357);
    			attr_dev(code2, "class", "svelte-t3irhz");
    			add_location(code2, file, 102, 32, 4490);
    			attr_dev(span3, "class", "hljs-attr");
    			add_location(span3, file, 106, 29, 4704);
    			attr_dev(span4, "class", "hljs-string");
    			add_location(span4, file, 106, 66, 4741);
    			add_location(div3, file, 106, 24, 4699);
    			attr_dev(span5, "class", "hljs-attr");
    			add_location(span5, file, 107, 29, 4824);
    			attr_dev(span6, "class", "hljs-string");
    			add_location(span6, file, 107, 72, 4867);
    			add_location(div4, file, 107, 24, 4819);
    			attr_dev(span7, "class", "hljs-attr");
    			add_location(span7, file, 108, 29, 4960);
    			attr_dev(span8, "class", "hljs-string");
    			add_location(span8, file, 108, 68, 4999);
    			add_location(div5, file, 108, 24, 4955);
    			attr_dev(span9, "class", "hljs-attr");
    			add_location(span9, file, 109, 29, 5084);
    			attr_dev(span10, "class", "hljs-string");
    			add_location(span10, file, 109, 74, 5129);
    			add_location(div6, file, 109, 24, 5079);
    			attr_dev(span11, "class", "hljs-attr");
    			add_location(span11, file, 110, 29, 5237);
    			attr_dev(span12, "class", "hljs-string");
    			add_location(span12, file, 110, 74, 5282);
    			add_location(div7, file, 110, 24, 5232);
    			attr_dev(span13, "class", "hljs-attr");
    			add_location(span13, file, 111, 29, 5386);
    			attr_dev(span14, "class", "hljs-string");
    			add_location(span14, file, 111, 74, 5431);
    			add_location(div8, file, 111, 24, 5381);
    			attr_dev(span15, "class", "hljs-attr");
    			add_location(span15, file, 112, 29, 5536);
    			attr_dev(span16, "class", "hljs-string");
    			add_location(span16, file, 112, 74, 5581);
    			add_location(div9, file, 112, 24, 5531);
    			attr_dev(span17, "class", "hljs-attr");
    			add_location(span17, file, 113, 29, 5686);
    			attr_dev(span18, "class", "hljs-string");
    			add_location(span18, file, 113, 69, 5726);
    			add_location(div10, file, 113, 24, 5681);
    			attr_dev(code3, "class", "lang-json flex flex-col pl-10 svelte-t3irhz");
    			add_location(code3, file, 105, 20, 4628);
    			add_location(pre2, file, 104, 16, 4601);
    			add_location(li0, file, 101, 12, 4452);
    			add_location(li1, file, 118, 12, 5903);
    			add_location(li2, file, 119, 12, 5991);
    			attr_dev(ul0, "class", "list-disc list-inside");
    			add_location(ul0, file, 100, 8, 4404);
    			attr_dev(h32, "class", "font-bold text-lg my-3 border-b border-gray-200");
    			attr_dev(h32, "id", "loan-amount-input");
    			add_location(h32, file, 123, 8, 6138);
    			attr_dev(p15, "class", "svelte-t3irhz");
    			add_location(p15, file, 126, 8, 6277);
    			add_location(li3, file, 128, 12, 6375);
    			add_location(li4, file, 129, 12, 6416);
    			add_location(li5, file, 130, 12, 6508);
    			attr_dev(ul1, "class", "list-disc list-inside");
    			add_location(ul1, file, 127, 8, 6327);
    			attr_dev(h33, "class", "font-bold text-lg my-3  border-b border-gray-200");
    			attr_dev(h33, "id", "months-input");
    			add_location(h33, file, 132, 8, 6597);
    			attr_dev(p16, "class", "svelte-t3irhz");
    			add_location(p16, file, 135, 8, 6727);
    			add_location(li6, file, 137, 12, 6825);
    			add_location(li7, file, 138, 12, 6866);
    			add_location(li8, file, 139, 12, 6955);
    			add_location(li9, file, 143, 12, 7149);
    			attr_dev(ul2, "class", "list-disc list-inside");
    			add_location(ul2, file, 136, 8, 6777);
    			attr_dev(h34, "class", "font-bold text-lg my-3  border-b border-gray-200");
    			attr_dev(h34, "id", "description-details");
    			add_location(h34, file, 145, 8, 7248);
    			add_location(li10, file, 149, 12, 7446);
    			attr_dev(ul3, "class", "list-disc list-inside");
    			add_location(ul3, file, 148, 8, 7398);
    			attr_dev(span19, "class", "hljs-attribute");
    			add_location(span19, file, 154, 19, 7632);
    			attr_dev(code4, "class", "svelte-t3irhz");
    			add_location(code4, file, 154, 13, 7626);
    			add_location(pre3, file, 154, 8, 7621);
    			attr_dev(span20, "class", "hljs-attribute");
    			add_location(span20, file, 156, 19, 7769);
    			attr_dev(span21, "class", "hljs-comment");
    			add_location(span21, file, 156, 92, 7842);
    			attr_dev(code5, "class", "svelte-t3irhz");
    			add_location(code5, file, 156, 13, 7763);
    			add_location(pre4, file, 156, 8, 7758);
    			attr_dev(span22, "class", "hljs-built_in");
    			add_location(span22, file, 158, 26, 7931);
    			attr_dev(span23, "class", "hljs-built_in");
    			add_location(span23, file, 158, 77, 7982);
    			attr_dev(span24, "class", "hljs-comment");
    			add_location(span24, file, 158, 120, 8025);
    			attr_dev(code6, "class", "svelte-t3irhz");
    			add_location(code6, file, 158, 13, 7918);
    			add_location(pre5, file, 158, 8, 7913);
    			attr_dev(h35, "class", "font-bold text-lg my-3");
    			attr_dev(h35, "id", "confirm-button");
    			add_location(h35, file, 160, 8, 8096);
    			attr_dev(p17, "class", "svelte-t3irhz");
    			add_location(p17, file, 161, 8, 8180);
    			attr_dev(div11, "class", "my-5 border-b border-gray-300 py-5");
    			add_location(div11, file, 63, 4, 2644);
    			attr_dev(h22, "class", "font-bold text-xl my-3");
    			attr_dev(h22, "id", "what-will-i-be-assessed-on");
    			add_location(h22, file, 164, 4, 8270);
    			add_location(hr1, file, 167, 4, 8390);
    			add_location(li11, file, 169, 8, 8437);
    			add_location(li12, file, 170, 8, 8474);
    			add_location(li13, file, 171, 8, 8519);
    			add_location(li14, file, 172, 8, 8605);
    			add_location(li15, file, 173, 8, 8660);
    			attr_dev(ol, "class", "list-decimal");
    			add_location(ol, file, 168, 4, 8402);
    			attr_dev(p18, "class", "svelte-t3irhz");
    			add_location(p18, file, 175, 4, 8727);
    			add_location(hr2, file, 176, 4, 8753);
    			attr_dev(h23, "class", "font-bold text-xl my-3");
    			attr_dev(h23, "id", "questions");
    			add_location(h23, file, 177, 4, 8765);
    			add_location(hr3, file, 178, 4, 8835);
    			attr_dev(p19, "class", "svelte-t3irhz");
    			add_location(p19, file, 179, 4, 8847);
    			attr_dev(p20, "class", "svelte-t3irhz");
    			add_location(p20, file, 183, 4, 9038);
    			attr_dev(h11, "class", "text-center text-3xl my-5");
    			attr_dev(h11, "id", "good-luck-");
    			add_location(h11, file, 184, 4, 9064);
    			attr_dev(div12, "class", "w-11/12 mx-auto bg-white border border-gray-400 p-10 mt-10");
    			add_location(div12, file, 12, 0, 200);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div0, anchor);
    			insert_dev(target, t0, anchor);
    			insert_dev(target, div12, anchor);
    			append_dev(div12, p0);
    			append_dev(p0, img0);
    			append_dev(div12, t1);
    			append_dev(div12, h10);
    			append_dev(div12, t3);
    			append_dev(div12, hr0);
    			append_dev(div12, t4);
    			append_dev(div12, div1);
    			append_dev(div1, p1);
    			append_dev(div1, t6);
    			append_dev(div1, p2);
    			append_dev(p2, t7);
    			append_dev(p2, a0);
    			append_dev(p2, t9);
    			append_dev(div1, t10);
    			append_dev(div1, p3);
    			append_dev(div12, t12);
    			append_dev(div12, h20);
    			append_dev(div12, t14);
    			append_dev(div12, div2);
    			append_dev(div2, p4);
    			append_dev(p4, t15);
    			append_dev(p4, a1);
    			append_dev(div2, t17);
    			append_dev(div2, p5);
    			append_dev(div2, t19);
    			append_dev(div2, pre0);
    			append_dev(pre0, code0);
    			append_dev(code0, t20);
    			append_dev(code0, span0);
    			append_dev(div2, t22);
    			append_dev(div2, p6);
    			append_dev(div2, t24);
    			append_dev(div2, pre1);
    			append_dev(pre1, code1);
    			append_dev(code1, t25);
    			append_dev(code1, span1);
    			append_dev(code1, span2);
    			append_dev(div2, t28);
    			append_dev(div2, p7);
    			append_dev(p7, t29);
    			append_dev(p7, a2);
    			append_dev(p7, t31);
    			append_dev(div12, t32);
    			append_dev(div12, h21);
    			append_dev(div12, t34);
    			append_dev(div12, div11);
    			append_dev(div11, p8);
    			append_dev(p8, img1);
    			append_dev(div11, t35);
    			append_dev(div11, p9);
    			append_dev(div11, t37);
    			append_dev(div11, p10);
    			append_dev(div11, t39);
    			append_dev(div11, p11);
    			append_dev(div11, t41);
    			append_dev(div11, h30);
    			append_dev(div11, t43);
    			append_dev(div11, p12);
    			append_dev(p12, t44);
    			append_dev(p12, a3);
    			append_dev(div11, t46);
    			append_dev(div11, p13);
    			append_dev(div11, t48);
    			append_dev(div11, h31);
    			append_dev(div11, t50);
    			append_dev(div11, p14);
    			append_dev(div11, t52);
    			append_dev(div11, ul0);
    			append_dev(ul0, li0);
    			append_dev(li0, t53);
    			append_dev(li0, code2);
    			append_dev(li0, t55);
    			append_dev(li0, pre2);
    			append_dev(pre2, code3);
    			append_dev(code3, div3);
    			append_dev(div3, span3);
    			append_dev(div3, t57);
    			append_dev(div3, span4);
    			append_dev(div3, t59);
    			append_dev(code3, t60);
    			append_dev(code3, div4);
    			append_dev(div4, span5);
    			append_dev(div4, t62);
    			append_dev(div4, span6);
    			append_dev(div4, t64);
    			append_dev(code3, t65);
    			append_dev(code3, div5);
    			append_dev(div5, span7);
    			append_dev(div5, t67);
    			append_dev(div5, span8);
    			append_dev(div5, t69);
    			append_dev(code3, t70);
    			append_dev(code3, div6);
    			append_dev(div6, span9);
    			append_dev(div6, t72);
    			append_dev(div6, span10);
    			append_dev(div6, t74);
    			append_dev(code3, t75);
    			append_dev(code3, div7);
    			append_dev(div7, span11);
    			append_dev(div7, t77);
    			append_dev(div7, span12);
    			append_dev(div7, t79);
    			append_dev(code3, t80);
    			append_dev(code3, div8);
    			append_dev(div8, span13);
    			append_dev(div8, t82);
    			append_dev(div8, span14);
    			append_dev(div8, t84);
    			append_dev(code3, t85);
    			append_dev(code3, div9);
    			append_dev(div9, span15);
    			append_dev(div9, t87);
    			append_dev(div9, span16);
    			append_dev(div9, t89);
    			append_dev(code3, t90);
    			append_dev(code3, div10);
    			append_dev(div10, span17);
    			append_dev(div10, t92);
    			append_dev(div10, span18);
    			append_dev(li0, t94);
    			append_dev(ul0, t95);
    			append_dev(ul0, li1);
    			append_dev(ul0, t97);
    			append_dev(ul0, li2);
    			append_dev(div11, t99);
    			append_dev(div11, h32);
    			append_dev(div11, t101);
    			append_dev(div11, p15);
    			append_dev(div11, t103);
    			append_dev(div11, ul1);
    			append_dev(ul1, li3);
    			append_dev(ul1, t105);
    			append_dev(ul1, li4);
    			append_dev(ul1, t107);
    			append_dev(ul1, li5);
    			append_dev(div11, t109);
    			append_dev(div11, h33);
    			append_dev(div11, t111);
    			append_dev(div11, p16);
    			append_dev(div11, t113);
    			append_dev(div11, ul2);
    			append_dev(ul2, li6);
    			append_dev(ul2, t115);
    			append_dev(ul2, li7);
    			append_dev(ul2, t117);
    			append_dev(ul2, li8);
    			append_dev(ul2, t119);
    			append_dev(ul2, li9);
    			append_dev(div11, t121);
    			append_dev(div11, h34);
    			append_dev(div11, t123);
    			append_dev(div11, ul3);
    			append_dev(ul3, li10);
    			append_dev(div11, t125);
    			append_dev(div11, pre3);
    			append_dev(pre3, code4);
    			append_dev(code4, span19);
    			append_dev(code4, t127);
    			append_dev(div11, t128);
    			append_dev(div11, pre4);
    			append_dev(pre4, code5);
    			append_dev(code5, span20);
    			append_dev(code5, t130);
    			append_dev(code5, span21);
    			append_dev(div11, t132);
    			append_dev(div11, pre5);
    			append_dev(pre5, code6);
    			append_dev(code6, t133);
    			append_dev(code6, span22);
    			append_dev(code6, t135);
    			append_dev(code6, span23);
    			append_dev(code6, t137);
    			append_dev(code6, span24);
    			append_dev(div11, t139);
    			append_dev(div11, h35);
    			append_dev(div11, t141);
    			append_dev(div11, p17);
    			append_dev(div12, t143);
    			append_dev(div12, h22);
    			append_dev(div12, t145);
    			append_dev(div12, hr1);
    			append_dev(div12, t146);
    			append_dev(div12, ol);
    			append_dev(ol, li11);
    			append_dev(ol, t148);
    			append_dev(ol, li12);
    			append_dev(ol, t150);
    			append_dev(ol, li13);
    			append_dev(ol, t152);
    			append_dev(ol, li14);
    			append_dev(ol, t154);
    			append_dev(ol, li15);
    			append_dev(div12, t156);
    			append_dev(div12, p18);
    			append_dev(div12, t158);
    			append_dev(div12, hr2);
    			append_dev(div12, t159);
    			append_dev(div12, h23);
    			append_dev(div12, t161);
    			append_dev(div12, hr3);
    			append_dev(div12, t162);
    			append_dev(div12, p19);
    			append_dev(div12, t164);
    			append_dev(div12, p20);
    			append_dev(div12, t166);
    			append_dev(div12, h11);
    		},
    		p: noop,
    		i: noop,
    		o: noop,
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div0);
    			if (detaching) detach_dev(t0);
    			if (detaching) detach_dev(div12);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$1.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance$1($$self, $$props) {
    	let { $$slots: slots = {}, $$scope } = $$props;
    	validate_slots('Readme', slots, []);
    	const writable_props = [];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== '$$' && key !== 'slot') console.warn(`<Readme> was created with unknown prop '${key}'`);
    	});

    	return [];
    }

    class Readme extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$1, create_fragment$1, safe_not_equal, {});

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "Readme",
    			options,
    			id: create_fragment$1.name
    		});
    	}
    }

    /* src\App.svelte generated by Svelte v3.41.0 */

    function create_fragment(ctx) {
    	let readme;
    	let current;
    	readme = new Readme({ $$inline: true });

    	const block = {
    		c: function create() {
    			create_component(readme.$$.fragment);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			mount_component(readme, target, anchor);
    			current = true;
    		},
    		p: noop,
    		i: function intro(local) {
    			if (current) return;
    			transition_in(readme.$$.fragment, local);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(readme.$$.fragment, local);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			destroy_component(readme, detaching);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance($$self, $$props, $$invalidate) {
    	let { $$slots: slots = {}, $$scope } = $$props;
    	validate_slots('App', slots, []);
    	const writable_props = [];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== '$$' && key !== 'slot') console.warn(`<App> was created with unknown prop '${key}'`);
    	});

    	$$self.$capture_state = () => ({ Readme });
    	return [];
    }

    class App extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance, create_fragment, safe_not_equal, {});

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "App",
    			options,
    			id: create_fragment.name
    		});
    	}
    }

    const app = new App({
        target: document.body,
        props: {}
    });

    return app;

}());
//# sourceMappingURL=bundle.js.map
