/* this file is needed for svelte-check 
* https://github.com/sveltejs/language-tools/issues/397
* we export the options so rollup can also use the same options
*/


const sveltePreprocess = require('svelte-preprocess');

const options = {
    sourceMap: true, // "you would always want sourcemaps for the IDE" – dummdidumm
    defaults: {
        script: "typescript",
        style: "postcss",
    },
    postcss: {
        plugins: [
            require("tailwindcss")(),
            require('autoprefixer')()
        ]
    }
};

module.exports = {
    preprocess: sveltePreprocess(options),
    options,
}
